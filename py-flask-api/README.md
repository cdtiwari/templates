# REST API
[![N|Solid](https://www.ebi.ac.uk/eccb/2016/wp-content/uploads/2016/03/EMBL-EBI.png)](https://www.ebi.ac.uk/)
## Get Started
To show off the features of Flask-RESTPlus I prepared a small demo application. It’s a part of an API for a blogging platform, which allows you to manage blog posts and categories.
### Prerequisites:
  - Virtualenv
  - GIT
  - Python 2/3

**To download and start the demo application issue the following commands. First clone the application code into any directory on your disk:**


  `$ cd /path/to/my/workspace/`

  `$ git clone git@gitlab.ebi.ac.uk:cdtiwari/templates.git`

  `$ cd templates/py-flask-api`

**Create a virtual Python environment in a directory named venv, activate the virtualenv and install required dependencies using pip:**


`$ virtualenv -p <which python3 | Python path> venv`

`$ source venv/bin/activate`

`(venv) $ pip install -r requirements.txt`

**Now let’s set up the app for development and start it:**


`(venv) $ python setup.py develop`

`(venv) $ python rest_api_demo/app.py`

**OK, everything should be ready. In your browser, open the URL** http://localhost:8888/api/
